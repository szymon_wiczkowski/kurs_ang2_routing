import { Component, OnInit } from '@angular/core';
import { Movie } from './movie';
import { FakeDbService } from '../fake-db.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {

  }
}
