import { Injectable } from '@angular/core';
import { Movie } from './movies/movie';
import { debug } from 'util';

@Injectable({
  providedIn: 'root'
})
export class FakeDbService {

  private movies = [
    new Movie(1, 'Rouge One', 2016, 8.1),
    new Movie(2, 'Matrix', 1999, 9.5),
    new Movie(3, 'Lord of the rings', 2001, 7.8),
    new Movie(4, 'Titanic', 1997, 6),
    new Movie(5, 'Terminator 2', 1991, 8.2),
    new Movie(6, 'Pulp Fiction', 1994, 8),
    new Movie(7, 'Lion King', 1994, 7.5)];

  constructor() { }

  // --spec false

  getMovies() {
    return this.movies;
  }

  getMovieById(movieId: number) {
    return this.movies.find(m => m.id === movieId);
  }
}

