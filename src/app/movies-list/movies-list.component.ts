import { Component, OnInit } from '@angular/core';
import { FakeDbService } from '../fake-db.service';
import { Movie } from '../movies/movie';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
  movies = new Array<Movie>();
  newMovieTitle: string;
  newMovieYear: number;

  constructor(private fakeDb: FakeDbService) { }

  ngOnInit() {
    this.movies = this.fakeDb.getMovies();
  }

  addMovie() {
    this.movies.push(
      new Movie(
        this.movies.length,
        this.newMovieTitle,
        this.newMovieYear,
        0));
    this.newMovieYear = null;
    this.newMovieTitle = '';
  }

  removeMovie(title: string) {
    // remove logic
    this.movies.splice(0, 1);
  }
}
